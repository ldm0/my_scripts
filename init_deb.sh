./daily_deb.sh
#when add-apt-repository can't be found
#On 12.04 and earlier, install the python-software-properties package:
#sudo apt-get install python-software-properties
#On 14.04 and later:
#sudo apt-get install software-properties-common
echo "sudo apt-get -y install build-essential software-properties-common python-software-properties"
sudo apt-get -y install build-essential software-properties-common python-software-properties >&-
wait
echo "sudo apt-get -y install vim gcc gdb g++ git p7zip-full wget curl cmake tmux"
sudo apt-get -y install vim gcc gdb g++ git p7zip-full wget curl cmake tmux >&-
wait
echo "sudo apt-get -y install libcunit1 libcunit1-doc libcunit1-dev openssl libncurses5-dev libncursesw5-dev"
sudo apt-get -y install libcunit1 libcunit1-doc libcunit1-dev openssl libncurses5-dev libncursesw5-dev  >&-
wait
echo "sudo apt-get -y install python python3 python3-pip python-pip"
sudo apt-get -y install python python3 python3-pip python-pip >&-
wait

